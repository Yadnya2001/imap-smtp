from socket import *
import base64
def connect():
    ip = gethostbyname("new.toad.com")
    s = socket(AF_INET, SOCK_STREAM)
    resp = s.connect((ip, 25))
    msg = "HELO new.toad.com\r\n"
    s.send(msg.encode())
    resp = s.recv(8000).decode()
    a = resp.split(" ")[0]
    b = resp.split("\r\n")[1].split(" ")[0]
    if (a == "220" and b == "250"):
        return s
    else:
        print("UNABLE TO CONNECT\r\n")
        return 0


def mail_ids():
    sender = str(input("Enter your mail id : "))
    print("for multiple receivers give COMMA SEPARATED mail ids")
    receiver = str(input("Enter receivers mail id :"))
    print("please enter \".\" if you do not want to add in CC\n")
    cc = str(input("Enter CC recipient: "))
    mails = [sender, receiver, cc]
    return mails

def file_attachment_paths():
    attach_path = []
    print("if do not want to attach any file give \".\" as a input")
    path = str(input("enter attachment file path : "))
    attach_path.append(path)
    if(attach_path[0] == "."):
        return attach_path
    print("A : add attachment\r\nB: DONE\r\n")
    choice = str(input("choice : "))
    while(choice == 'A'):
        path = str(input("enter attachment file path : "))
        attach_path.append(path)
        print("A : add attachment\r\nB: DONE\r\n")
        choice = str(input("choice : "))
    print(attach_path)
    return attach_path

def mail_subject():
    subject = str(input("SUBJECT:"))
    return subject

def mail_body():
    print("------------enter body of th mail----------------\r\n")
    body = str(input())
    return body

def mail_build_body(body, attach_path):
    attachments = []
    text = ["plain", "html"]
    image = ["jpg", "jpeg", "gif", "png"]
    audio = ["wav", "mp3", "mpeg", "x-wav", "m4a", "webm"]
    video = ["mp4"]
    mime = "MIME-Version: 1.0\r\n"
    contentType = "Content-type: multipart/mixed; boundary=\"MyBoundaryString\"\r\n\r\n"
    boundary = "--MyBoundaryString\r\n"
    build_body = mime + contentType
    c_type = "Content-Type: text/plain; charset=\"UTF-8\"\r\n\r\n"
    attachment_1 =build_body + boundary + c_type + body + "\r\n\r\n"
    attachments.append(attachment_1.encode())
    for i in attach_path:
        file_name = i.split("/")[-1]
        file_format_index = file_name.rfind(".")
        file_format = file_name[file_format_index + 1:]
        if(file_format in text):
            content_type = "Content-Type: text/" + file_format + "; name=\"" + file_name + "\"\r\n"
        elif(file_format in image):
            content_type = "Content-Type: image/" + file_format + "; name=\"" + file_name + "\"\r\n"
        elif (file_format in audio):
            content_type = "Content-Type: audio/" + file_format + "; name=\"" + file_name + "\"\r\n"
        elif (file_format in video):
            content_type = "Content-Type: video/" + file_format + "; name=\"" + file_name + "\"\r\n"
        else:
            content_type = "Content-Type: application/" + file_format + "; name=\"" + file_name + "\"\r\n"

        attachment = boundary + content_type + "Content-Disposition: attachment; filename=\"" + file_name + "\"\r\n"
        attachment = attachment + "Content-Transfer-Encoding: base64\r\n\r\n"
        file = open(i, "rb")
        a = file.read()
        b = base64.b64encode(a)
        attachment = attachment.encode() + b + "\r\n\r\n".encode()
        attachments.append(attachment)

    attachment_2 = boundary.encode() + "--\r\n.\r\n".encode()
    attachments.append(attachment_2)
    return attachments

def set_sender_receiver(mails, s):
    sender_flag = receiver_flag = 0
    mail_from = mails[0]
    rcpt_to = mails[1]
    mail_from = "mail from:<" + mail_from + ">\r\n"
    print(mail_from)
    s.send(mail_from.encode())
    resp = s.recv(2048).decode()
    if("ok" in resp):
        sender_flag = 1
    rcpt_to = "rcpt to:<" + rcpt_to + ">\r\n"
    print(rcpt_to)
    s.send(rcpt_to.encode())
    resp = s.recv(8000).decode()
    if("ok" in resp):
        receiver_flag = 1
    if(sender_flag == 1 and receiver_flag == 1):
        return 1
    else:
        return 0


def send_mail(mails, subject, body, attach_path, s):
    data = "DATA\r\n"
    s.send(data.encode())
    resp = s.recv(2048).decode()
    if("Enter mail" not in resp):
        print("MAIL NOT SENT\n")
        return 0
    build_body = ""
    build_body = "Subject: " + subject +"\r\nFrom:" + mails[0] + "\r\nto:" + mails[1] + "\r\n"
    if(mails[2] != "."):
        build_body = "Cc:" + mails[2] + "\r\n"
    if(attach_path[0] != "."):
        print("path is : ",attach_path)
        msg = mail_build_body(body, attach_path)
        body = ""
        for i in msg:
            body = body + i.decode()
        body = body.encode()
        body = build_body.encode() + body
        s.send(body)
        resp = s.recv(2048).decode()
        print(resp)
        return 1
    build_body = build_body + "Content-Type: text/plain; charset=\"UTF-8\"\r\n\r\n" + body + "\r\n\r\n"
    build_body = build_body + ".\r\n"
    s.send(build_body.encode())
    resp = s.recv(2048).decode()
    if("Message accepted" in resp):
        resp = resp.split(" ")[2]
        print("msg send with id ", resp)
        return 1
    else:
        print("msg not sent")
        return 0

#test
'''def add_attachment(build_body, body, attach_path):
    mime = "MIME-Version: 1.0\r\n"
    contentType = "Content-type: multipart/mixed; boundary=\"MyBoundaryString\"\r\n\r\n"
    boundary ="--MyBoundaryString\r\n"
    build_body = build_body + mime + contentType
    c_type = "Content-Type: text/plain; charset=\"UTF-8\"\r\n\r\n"
    attachment_1 = boundary + c_type + body + "\r\n\r\n"
    attachment_2 = boundary + "Content-Type: image/jpeg; name=\"1.mp4\"\r\n"
    attachment_2 = attachment_2 + "Content-Disposition: attachment; filename=\"1.mp4\"\r\n"
    attachment_2 = attachment_2 + "Content-Transfer-Encoding: base64\r\n\r\n"
    file = open(attach_path, "rb")
    a = file.read()
    b = base64.b64encode(a)
    attachment_2 = attachment_2.encode() + b
    msg = build_body.encode() + attachment_1.encode() + attachment_2 + "\r\n".encode() + boundary.encode() + "--\r\n.\r\n".encode()
    return msg'''
