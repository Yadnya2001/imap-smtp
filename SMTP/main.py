from inputs import *

mails = mail_ids()
subject = mail_subject()
body = mail_body()
attach_path = file_attachment_paths()
s = connect()
recv_mail_ids = mails[1].split(",")
cc_mail_ids = mails[2].split(",")

for i in recv_mail_ids:
    s = connect()
    mail_id_set = [mails[0], i, mails[2]]
    if(set_sender_receiver(mail_id_set, s)):
        if(send_mail(mails, subject, body, attach_path, s)):
            print("sending => ", i)

if(mails[2] != "." and mails[2] != ""):
    for i in cc_mail_ids:
        s = connect()
        mail_id_set = [mails[0], i, mails[2]]
        if (set_sender_receiver(mail_id_set, s)):
            if (send_mail(mails, subject, body, attach_path, s)):
                print("sending => ", i)
